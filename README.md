# MasquesRINGOT

## Description

TP étudiant M2 : Formulaire avec liaisons entre activités.

## Auteur

- [Ringot Arthur](https://gitlab.com/aringot1612)

## Aperçus :

### Mode portrait
#### Menu principal
<br>![Aperçu : Menu principal (portrait)](images/main_portrait.png)

#### Menu Infos essentielles
<br>![Aperçu : Infos essentielles (portrait)](images/basic_portrait.png)

#### Menu Infos d'adresse
<br>![Aperçu :  Infos d'adresse (portrait)](images/address_portrait.png)

### Mode paysage
#### Menu principal
<br>![Aperçu : Menu principal (paysage)](images/main_landscape.png)

#### Menu Infos essentielles
<br>![Aperçu : Infos essentielles (paysage)](images/basic_landscape.png)

#### Menu Infos d'adresse
<br>![Aperçu :  Infos d'adresse (paysage)](images/address_landscape.png)