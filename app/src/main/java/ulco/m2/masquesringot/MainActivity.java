package ulco.m2.masquesringot;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    // Constantes permettant de reconnaître les valeurs envoyées et reçues entre activités.
    public final static String MSG_NAME = "ulco.m2.masquesringot.MSG_NAME";
    public final static String MSG_FIRST_NAME = "ulco.m2.masquesringot.MSG_FIRST_NAME";
    public final static String MSG_PHONE = "ulco.m2.masquesringot.MSG_PHONE";
    public final static String MSG_STREET_NB = "ulco.m2.masquesringot.MSG_STREET_NB";
    public final static String MSG_STREET_NAME = "ulco.m2.masquesringot.MSG_STREET_NAME";
    public final static String MSG_POSTAL_CODE = "ulco.m2.masquesringot.MSG_POSTAL_CODE";
    public final static String MSG_CITY_NAME = "ulco.m2.masquesringot.MSG_CITY_NAME";

    // Constantes permettant de reconnaître quelle activité a répondu.
    private final static int REQUSET_CODE_BASICS = 1;
    private final static int REQUSET_CODE_ADDRESS = 2;

    // Boutons.
    private Button goToModifyBasics;
    private Button goToModifyAddress;

    // TextView stockant des informations à utiliser.
    private TextView name;
    private TextView firstName;
    private TextView phone;
    private TextView streetNb;
    private TextView streetName;
    private TextView postalCode;
    private TextView cityName;

    /** Est appelée à la création de l'activité.
     *
     * @param savedInstanceState Bundle stockant des valeurs si utilisé auparavant.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Ajout d'un icone dans la barre d'application.
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.mipmap.icon_foreground);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Initialisation des différents composants de l'activité à utiliser.
        findViews();
        // Création des Listenners pour boutons.
        createEventHandlers();
        // Récupération de données éventuelles depuis le bundle.
        retrieveDataFromBundle(savedInstanceState);
    }

    /** Permet d'initialiser les composants de l'activité destinés à être utilisé par la suite. */
    private void findViews(){
        this.goToModifyBasics = findViewById(R.id.modifyBasics);
        this.goToModifyAddress = findViewById(R.id.modifyAddress);
        this.name = findViewById(R.id.name);
        this.firstName = findViewById(R.id.firstName);
        this.phone = findViewById(R.id.phone);
        this.streetNb = findViewById(R.id.streetNb);
        this.streetName = findViewById(R.id.streetName);
        this.postalCode = findViewById(R.id.postalCode);
        this.cityName = findViewById(R.id.cityName);
    }

    /** Permet d'associer à chaque bouton son listener. */
    private void createEventHandlers(){
        goToModifyBasics.setOnClickListener(v -> goToModifyBasicsActivity());
        goToModifyAddress.setOnClickListener(v -> goToModifyAddressActivity());
    }

    /** Est appelée lorsque l'utilisateur souhaite modifier les informations essentielles. */
    private void goToModifyBasicsActivity(){
        Intent msg = new Intent(this, ulco.m2.masquesringot.ModifyBasicsActivity.class);
        msg.putExtra(MSG_NAME, name.getText().toString());
        msg.putExtra(MSG_FIRST_NAME, firstName.getText().toString());
        msg.putExtra(MSG_PHONE, phone.getText().toString());
        startActivityForResult(msg, REQUSET_CODE_BASICS);
    }

    /** Est appelée lorsque l'utilisateur souhaite modifier les informations d'adresse. */
    private void goToModifyAddressActivity(){
        Intent msg = new Intent(this, ulco.m2.masquesringot.ModifyAddressActivity.class);
        msg.putExtra(MSG_STREET_NB, streetNb.getText().toString());
        msg.putExtra(MSG_STREET_NAME, streetName.getText().toString());
        msg.putExtra(MSG_POSTAL_CODE, postalCode.getText().toString());
        msg.putExtra(MSG_CITY_NAME, cityName.getText().toString());
        startActivityForResult(msg, REQUSET_CODE_ADDRESS);
    }

    /** Est appelée lorsqu'une activité appelée renvoie un résultat.
     *
     * @param requestCode Le code correspondant à l'activité ayant renvoyé un résultat.
     * @param resultCode Le code du résultat.
     * @param data Les données associées aux retour.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Si le code résultat est correct, l'activité a été quittée dans le but d'enregistrer les modifications.
        if(resultCode == RESULT_OK)
            // Si l'activité ayant émis le code concerne la modification des infos essentielles.
            if(requestCode == REQUSET_CODE_BASICS){
                // Mise à jour des valeurs concernées.
                name.setText(data.getStringExtra(MSG_NAME));
                firstName.setText(data.getStringExtra(MSG_FIRST_NAME));
                phone.setText(data.getStringExtra(MSG_PHONE));
            }
            // Si l'activité ayant émis le code concerne la modification des infos d'adresse.
            else if(requestCode == REQUSET_CODE_ADDRESS){
                // Mise à jour des valeurs concernées.
                streetNb.setText(data.getStringExtra(MSG_STREET_NB));
                streetName.setText(data.getStringExtra(MSG_STREET_NAME));
                postalCode.setText(data.getStringExtra(MSG_POSTAL_CODE));
                cityName.setText(data.getStringExtra(MSG_CITY_NAME));
            }
    }

    /** Permet de sauvegarder nos valeurs lors d'un changement d'orientation dans un bundle.
     *
     * @param outState Le bundle stockant nos valeurs à sauvegarder.
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(MSG_NAME, name.getText().toString());
        outState.putString(MSG_FIRST_NAME, firstName.getText().toString());
        outState.putString(MSG_PHONE, phone.getText().toString());
        outState.putString(MSG_STREET_NB, streetNb.getText().toString());
        outState.putString(MSG_STREET_NAME, streetName.getText().toString());
        outState.putString(MSG_POSTAL_CODE, postalCode.getText().toString());
        outState.putString(MSG_CITY_NAME, cityName.getText().toString());
        super.onSaveInstanceState(outState);
    }

    /** Permet de restaurer nos valeurs lors d'un changement d'orientation à partir du bundle.
     *
     * @param savedInstanceState Le bundle stockant nos valeurs à restaurer.
     */
    private void retrieveDataFromBundle(Bundle savedInstanceState){
        if (savedInstanceState != null) {
            name.setText(savedInstanceState.getString(MSG_NAME));
            firstName.setText(savedInstanceState.getString(MSG_FIRST_NAME));
            phone.setText(savedInstanceState.getString(MSG_PHONE));
            streetNb.setText(savedInstanceState.getString(MSG_STREET_NB));
            streetName.setText(savedInstanceState.getString(MSG_STREET_NAME));
            postalCode.setText(savedInstanceState.getString(MSG_POSTAL_CODE));
            cityName.setText(savedInstanceState.getString(MSG_CITY_NAME));
        }
    }
}