package ulco.m2.masquesringot;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class ModifyBasicsActivity extends AppCompatActivity {

    // Boutons.
    private Button validate;
    private Button cancel;

    // EditText stockant des informations à utiliser.
    private EditText name;
    private EditText firstName;
    private EditText phone;

    // Intent de résultat.
    private Intent res;

    /** Est appelé à la création de l'activité.
     *
     * @param savedInstanceState Bundle stockant des valeurs si utilisé auparavant.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_basics);
        this.res = new Intent(this, ulco.m2.masquesringot.ModifyBasicsActivity.class);
        findViews();
        createEventHandlers();
        retrieveDataFromBundle(savedInstanceState);
        retrieveDataFromIntent();
    }

    /** Permet d'initialiser les composants de l'activité destinés à être utilisé par la suite. */
    private void findViews(){
        this.validate = findViewById(R.id.validBasics);
        this.cancel = findViewById(R.id.cancelBasics);
        this.name = findViewById(R.id.modifyName);
        this.firstName = findViewById(R.id.modifyFirstName);
        this.phone = findViewById(R.id.modifyPhone);
    }

    /** Permet d'associer à chaque bouton son listener. */
    private void createEventHandlers(){
        validate.setOnClickListener(v -> goToMainActivity());
        cancel.setOnClickListener(v -> cancel());
    }

    /** Est appelée lorsque l'utilisateur souhaite retourner à l'activité principale avec sauvegarde des résultats. */
    private void goToMainActivity(){
        res.putExtra(MainActivity.MSG_NAME, name.getText().toString());
        res.putExtra(MainActivity.MSG_FIRST_NAME, firstName.getText().toString());
        res.putExtra(MainActivity.MSG_PHONE, phone.getText().toString());
        setResult(RESULT_OK, res);
        finish();
    }

    /** Est appelée lorsque l'utilisateur souhaite retourner à l'activité principale sans sauvegarde des résultats. */
    private void cancel(){
        setResult(RESULT_CANCELED, res);
        finish();
    }

    /** Permet de sauvegarder nos valeurs lors d'un changement d'orientation dans un bundle.
     *
     * @param outState Le bundle stockant nos valeurs à sauvegarder.
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(MainActivity.MSG_NAME, name.getText().toString());
        outState.putString(MainActivity.MSG_FIRST_NAME, firstName.getText().toString());
        outState.putString(MainActivity.MSG_PHONE, phone.getText().toString());
        super.onSaveInstanceState(outState);
    }

    /** Permet de restaurer nos valeurs lors d'un changement d'orientation à partir du bundle.
     *
     * @param savedInstanceState Le bundle stockant nos valeurs à restaurer.
     */
    private void retrieveDataFromBundle(Bundle savedInstanceState){
        if (savedInstanceState != null) {
            name.setText(savedInstanceState.getString(MainActivity.MSG_NAME));
            firstName.setText(savedInstanceState.getString(MainActivity.MSG_FIRST_NAME));
            phone.setText(savedInstanceState.getString(MainActivity.MSG_PHONE));
        }
    }

    /** Permet de récupérer les valeurs issues de l'Intent venant de l'activité principale. */
    private void retrieveDataFromIntent(){
        Intent response = getIntent();
        name.setText(response.getStringExtra(MainActivity.MSG_NAME));
        firstName.setText(response.getStringExtra(MainActivity.MSG_FIRST_NAME));
        phone.setText(response.getStringExtra(MainActivity.MSG_PHONE));
    }
}