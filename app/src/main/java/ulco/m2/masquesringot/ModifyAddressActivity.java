package ulco.m2.masquesringot;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class ModifyAddressActivity extends AppCompatActivity {

    // Boutons.
    private Button validate;
    private Button cancel;

    // EditText stockant des informations à utiliser.
    private EditText streetNb;
    private EditText streetName;
    private EditText postalCode;
    private EditText cityName;

    // Intent de résultat.
    private Intent res;

    /** Est appelé à la création de l'activité.
     *
     * @param savedInstanceState Bundle stockant des valeurs si utilisé auparavant.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_address);
        this.res = new Intent(this, ulco.m2.masquesringot.ModifyAddressActivity.class);
        // Initialisation des différents composants de l'activité à utiliser.
        findViews();
        // Création des Listenners pour boutons.
        createEventHandlers();
        // Récupération de données éventuelles depuis le bundle.
        retrieveDataFromBundle(savedInstanceState);
        // Récupération de données éventuelles depuis l'Intent issue de l'activité principale.
        retrieveDataFromIntent();
    }

    /** Permet d'initialiser les composants de l'activité destinés à être utilisé par la suite. */
    private void findViews(){
        this.validate = findViewById(R.id.validAddress);
        this.cancel = findViewById(R.id.cancelAddress);
        this.streetNb = findViewById(R.id.modifyStreetNb);
        this.streetName = findViewById(R.id.modifyStreetName);
        this.postalCode = findViewById(R.id.modifyPostalCode);
        this.cityName = findViewById(R.id.modifyCityName);
    }

    /** Permet d'associer à chaque bouton son listener. */
    private void createEventHandlers(){
        validate.setOnClickListener(v -> goToMainActivity());
        cancel.setOnClickListener(v -> cancel());
    }

    /** Est appelée lorsque l'utilisateur souhaite retourner à l'activité principale avec sauvegarde des résultats. */
    private void goToMainActivity(){
        res.putExtra(MainActivity.MSG_STREET_NB, streetNb.getText().toString());
        res.putExtra(MainActivity.MSG_STREET_NAME, streetName.getText().toString());
        res.putExtra(MainActivity.MSG_POSTAL_CODE, postalCode.getText().toString());
        res.putExtra(MainActivity.MSG_CITY_NAME, cityName.getText().toString());
        setResult(RESULT_OK, res);
        finish();
    }

    /** Est appelée lorsque l'utilisateur souhaite retourner à l'activité principale sans sauvegarde des résultats. */
    private void cancel(){
        setResult(RESULT_CANCELED, res);
        finish();
    }

    /** Permet de sauvegarder nos valeurs lors d'un changement d'orientation dans un bundle.
     *
     * @param outState Le bundle stockant nos valeurs à sauvegarder.
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(MainActivity.MSG_STREET_NB, streetNb.getText().toString());
        outState.putString(MainActivity.MSG_STREET_NAME, streetName.getText().toString());
        outState.putString(MainActivity.MSG_POSTAL_CODE, postalCode.getText().toString());
        outState.putString(MainActivity.MSG_CITY_NAME, cityName.getText().toString());
        super.onSaveInstanceState(outState);
    }

    /** Permet de restaurer nos valeurs lors d'un changement d'orientation à partir du bundle.
     *
     * @param savedInstanceState Le bundle stockant nos valeurs à restaurer.
     */
    private void retrieveDataFromBundle(Bundle savedInstanceState){
        if (savedInstanceState != null) {
            streetNb.setText(savedInstanceState.getString(MainActivity.MSG_STREET_NB));
            streetName.setText(savedInstanceState.getString(MainActivity.MSG_STREET_NAME));
            postalCode.setText(savedInstanceState.getString(MainActivity.MSG_POSTAL_CODE));
            cityName.setText(savedInstanceState.getString(MainActivity.MSG_CITY_NAME));
        }
    }

    /** Permet de récupérer les valeurs issues de l'Intent venant de l'activité principale. */
    private void retrieveDataFromIntent(){
        Intent response = getIntent();
        streetNb.setText(response.getStringExtra(MainActivity.MSG_STREET_NB));
        streetName.setText(response.getStringExtra(MainActivity.MSG_STREET_NAME));
        postalCode.setText(response.getStringExtra(MainActivity.MSG_POSTAL_CODE));
        cityName.setText(response.getStringExtra(MainActivity.MSG_CITY_NAME));
    }
}